function getk(x1,y1,x2,y2){
if(x2===x1)return {f:"x",v:0};
if(y1===y2)return {f:"y",v:0};

    return {f:"z",v:(Math.abs(y2)-y1)/(x2-x1)}

}
function getAng(obj1,obj2){
        if(obj1.f!=="z"&&obj2.f!=="z"&&obj1.f!==obj2.f){
            return Math.PI/2
        }
        k2=obj2.v;
        k1=obj1.v;
    return Math.atan((k2-k1)/(1+k1*k2))
}

function xy(cx,cz,dx,dz) {
    function pf(x) {
        return x*x;
    }
    if(dx===0){
        return{
            x:0,
            z:Math.sqrt(pf(cx)+pf(cz)),
        }

    }
    var x=Math.sqrt((pf(cx)+pf(cz))/(1+(pf(dz)/pf(dx))));
    var z=x*dz/dx


    return {
        x:x,
        z:z,
    }
}
function getAngle(x,z) {
    function pf(x) {
        return x*x;
    }
    var r=Math.sqrt(pf(x)+pf(z));
    var abg=Math.asin(Math.abs(z)/r);
    var s;
    if(x>0&&z<0){
        s=Math.PI*3/2
    }
    if(x>0&&z>0||x==0){
        s=0
    }
    if(x<0&&z>0){
        s=Math.PI/2
    }
    if(x<0&&z<0){
        s=Math.PI
    }

    return s+abg;
}
function getSubAng(start,end) {
    end-start
}

function getLinesAngel(x1,y1,x2,y2) {
    console.log(getAngle(x1,y1)-getAngle(x2,y2))
    return Math.atan(((y2*x1)-(y1*x2))/((x2*x1)+(y2*y1)))
}
function getAngle(x,z) {
    function pf(x) {
        return x*x;
    }
    var r=Math.sqrt(pf(x)+pf(z));
    var abg=Math.asin(Math.abs(z)/r);
    var s;
    if(x>0&&z<0){
        s=Math.PI*3/2
    }
    if(x>0&&z>0){
        s=0
    }
    if(x<0&&z>0){
        s=Math.PI/2
    }
    if(x<0&&z<0){
        s=Math.PI
    }
    if(x==0&&z>0){
        s=0

    }
    if(x==0&&z<0){
        s=Math.PI
    }
    if(z==0&&x>0){
        s=0
    }
    if(z==0&&x<0){
        s=Math.PI
    }
    return (s+abg);
}
console.log(getAngle(0,-1)*180/Math.PI)
console.log(getAngle(0,-1))